---
title: "Inferelator to integrate DAP-seq-based TF-gene interations with wood expression data"
author: "Torgeir R Hvidsten"
date: '`r format(Sys.time(), "%d.%m.%Y")`'
output:
  html_document:
    toc: true
    toc_float: true
    theme: yeti
    code_folding: hide
editor_options: 
  chunk_output_type: console
---

```{r setup, message=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(stringsAsFactors = FALSE)

library(tidyverse)
theme_set(theme_classic())
theme_update(plot.title = element_text(face="bold"))

library(cowplot)
library(DT)
library(pheatmap)

expr_file <- "data/AspWood_transcriptomics_Ptremula.txt"
net_file <- "data/DAPseq_ATAC-filtered-peaks_intersect_w_promoter2kb_20-May-2022.tsv"

time = TRUE
randomize <- FALSE

inferelator_output <- "network/DAP_ATAC_93x559_bootstrap100_w0.75_0.5_time" # On/off
```

### Import data

I'm using the network built from evidence of physical interaction between gene promoters and TFs from:

* DAP-seq
* ATAC-seq

This network is referred to as the **prior network**.

To zoom in on the network of genes that are activly regulated (turned on and off) during wood formations, I filtered the AspWood expression data to retain genes that meet both these criteria:

* Log2 TPM expression >= 3 in at least two samples in three of the four trees (same criteria for "on" as for the AspWood paper).
* Log2 TPM expression == 0 in at least three of the four trees ("off")

Finally, I only retain TFs with at least two target genes after the above expression criteria.

```{r, message=FALSE, warning=FALSE}

# Network
net <- read.delim(net_file, header = TRUE, sep = "\t")

if (randomize) {
  net$Potra02.Target <- net$Potra02.Target[ sample(1:nrow(net), nrow(net)) ]
}

no_TFs <- length(unique(net$Potra02.TF))
no_targets <- length(unique(net$Potra02.Target))
cat("Prior network: TFs:", no_TFs, " - Targets:", no_targets)

# Expression data
expr <- read.delim(expr_file, header = TRUE, sep = "\t")
samples_order <- colnames(expr)[2:ncol(expr)]

# Filtering - expression
expr <- expr %>% 
  gather (Samples, Expression, -1) %>%
  filter (Genes %in% c(net$Potra02.TF, net$Potra02.Target)) %>%
  separate(Samples, into = c("Trees", "Samples"), sep = "\\.") %>%
  mutate_at("Samples", as.numeric) %>%
  mutate(Expression = log2(Expression+1))

expressed_genes <- expr %>%
  filter(Expression >= 3) %>%
  group_by(Genes, Trees) %>%
  filter(n() >= 2) %>%
  summarise(Expression = max(Expression)) %>%
  group_by(Genes) %>%
  filter(n() >= 3) %>%
  pull(unique(Genes))

if (FALSE) { # Variance
  not_expressed_genes <- expr %>%
    group_by(Genes, Trees) %>%
    summarise(Variance = sd(Expression)) %>%
    filter(Variance >= 1) %>%
    group_by(Genes) %>%
    filter(n() >= 3) %>%
    pull(unique(Genes))
  
  expressed_genes <- intersect(expressed_genes, not_expressed_genes)
}

# * Log2 TPM expression == 0 in at least two samples in three of the four trees (criteria for "off").
if (TRUE) { # Off
  not_expressed_genes <- expr %>%
    filter(Expression == 0) %>%
    group_by(Genes, Trees) %>%
    filter(n() >=2) %>%
    summarise(Expression = max(Expression)) %>%
    group_by(Genes) %>%
    filter(n() >= 3) %>%
    pull(unique(Genes))
  
  expressed_genes <- intersect(expressed_genes, not_expressed_genes)
}

net <- net %>%
  filter(Potra02.TF %in% expressed_genes) %>%
  filter(Potra02.Target %in% expressed_genes)

expr <- expr %>%
  filter(Genes %in% expressed_genes)

no_TFs <- length(unique(net$Potra02.TF))
no_targets <- length(unique(net$Potra02.Target))
cat("Expression filter:\n  Prior network: TFs:", no_TFs, " - Targets:", no_targets, "\n")

# Filtering - targets
net <- net %>%
  group_by(Potra02.TF) %>%
  filter(n() >= 2)

expr <- expr %>%
  filter(Genes %in% c(net$Potra02.TF, net$Potra02.Target))

no_TFs <- length(unique(net$Potra02.TF))
no_targets <- length(unique(net$Potra02.Target))
cat("Target filter:\n  Prior network: TFs:", no_TFs, " - Targets:", no_targets, "\n")

# Annotations

gene.info <- read.delim2("data/gene_info.txt") %>%
  rename(regulator = gene_id, descr = description) %>%
  select(regulator, descr)

gene.info$descr <- gsub("\\S+\\|\\S+\\|\\S+\\s", "", gene.info$descr)
gene.info$descr <- gsub(" OS=.+$", "", gene.info$descr)

```

Plots of the expression of 10 random TFs and the distribution of the number of target genes for all TFs in the prior network.

```{r, message=FALSE, warning=FALSE, fig.height=15}

# Plotting

expressed_TFs <- expr %>%
  filter(Genes %in% net$Potra02.TF) %>%
  group_by(Genes) %>%
  summarise(Expression = max(Expression)) %>%
  arrange(desc(Expression)) %>%
  pull(Genes)

reprog_events <- tibble(Trees = paste0("T", c(rep(1,3),rep(2,3),rep(3,3),rep(4,3))), 
           xintercept = c(5.5, 12.5, 19.5, 5.5, 11.5, 19.5, 5.5, 14.5, 21.5, 5.5, 12.5, 20.5))

expr %>% filter(Genes %in% expressed_TFs[sample(1:length(expressed_TFs), 10)]) %>% # c(1, 5, 9, 14, 15, 20)
  mutate(Genes = factor(Genes, levels = expressed_TFs)) %>%
  ggplot(aes(x = Samples, y = Expression, col = Trees)) +
  geom_line() +
  geom_vline(data = reprog_events, mapping = aes(xintercept = xintercept), 
             linetype="dashed", color = "gray68", size=0.5) +
  facet_grid(rows = vars(Genes), cols = vars(Trees)) +
  theme(strip.text = element_text(size = 8, color = "black"), legend.position = "none")
```

```{r, message=FALSE, warning=FALSE}

net %>%
  group_by(Potra02.TF, Potra02.Target) %>%
  slice(1) %>%
  group_by(Potra02.TF) %>%
  summarise(Targets = n()) %>%
  ggplot(aes(x = Targets)) +
  geom_histogram(bins = 100) +
  labs(x = "Number of target genes", y = "Number of TFs")

```

### Run Inferelator

Inferelator infers networks from evidence of TF-gene interaction (prior network) and expression data in two steps:

* Based on the expression profiles of its gene targets, an activity profile is generated for each TF. 
* TF-gene regulatory links are predicted using regularized regression and is based only on the activity profiles of the TFs. The expression profiles of the TFs are not used at all! I.e. it asks: what combination of TF activity profiles predict the expression of this gene? Boostrapping is used to compute confidence scores for each link.

Inferelator is ran separatly on a server. The code below generates the input files.

```{r, message=FALSE, warning=FALSE}

# Expression to file
expr_table <- expr %>%
  rowwise() %>%
  mutate(Samples = paste(Trees, Samples, sep = ".")) %>%
  select(-Trees) %>%
  spread(Samples, Expression) %>%
  as.data.frame() %>%
  `rownames<-`(.[,1]) %>%
  select(-Genes)

write.table(expr_table, "AspWood-expression.tsv", quote = FALSE, sep = "\t", row.names = TRUE, col.names=NA)

if (time) {
  expr_meta <- data.frame(isTs = c(TRUE), is1stLast = c(NA), prevCol = c(NA), del.t = c(1), condName = colnames(expr_table)) %>%
    separate(condName, into = c("Tree", "Time"), sep = "\\.", remove = FALSE) %>%
    mutate(Time = as.numeric(Time)) %>%
    arrange(Tree, Time)
  
  for (i in 1:nrow(expr_meta)) {
    if (expr_meta$Time[i] == 1) {
      expr_meta$is1stLast[i] <- "f"
    } else if (i == nrow(expr_meta) || expr_meta$Tree[i] != expr_meta$Tree[i+1]) {
      expr_meta$is1stLast[i] <- "l"
      expr_meta$prevCol[i] <- expr_meta$condName[i-1]
    } else {
      expr_meta$is1stLast[i] <- "m"
      expr_meta$prevCol[i] <- expr_meta$condName[i-1]
    }
  }
  
  expr_meta <- expr_meta %>%
    select(-Tree, -Time)
  
} else {
  expr_meta <- data.frame(isTs = c(FALSE), is1stLast = c(NA), prevCol = c(NA), del.t = c(NA), condName = colnames(expr_table))
}
write.table(expr_meta, "AspWood-expression-meta.tsv", quote = FALSE, sep = "\t")

# Prior to file
prior <- net %>%
  select(Potra02.TF, Potra02.Target) %>%
  group_by(Potra02.TF, Potra02.Target) %>%
  slice(1) %>%
  mutate(Interaction = c(1)) %>%
  spread(Potra02.TF, Interaction, fill = 0) %>%
  as.data.frame() %>%
  `rownames<-`(.[,1]) %>%
  select(-Potra02.Target)

write.table(prior, "AspWood-prior.tsv", quote = FALSE, sep = "\t", row.names = TRUE, col.names=NA)

# TFs to file
write.table(data.frame(colnames(prior)), "AspWood-tf-names.tsv", row.names = FALSE, col.names = FALSE)

```

### Inferelator network

We can now use the prior network as a gold standard to evaluate to what degree the Inferelator network retain the interactions in the prior network and therefore to what degree expression data is consistent with the physical interaction data (prior). 

Precision (fraction of predicted TF-gene interactions in the prior) and recall (fraction of prior TF-gene interactions retained by predictions). Confidence scores > 0.5 are used in the final network.

* Final network: Performance on the entire dataset
* Holdout set: Performence on 20% of the genes not used for training the network 
* Randomized: Performance of random guessing

```{r, message=FALSE, warning=FALSE}

net2 <- read.delim(paste0(inferelator_output, "/network.tsv.gz"), header = TRUE, sep = "\t")
net2$Type <- c("Final network")

conf_thr <- 0.5
recall_thr <- net2$recall[sum(net2$combined_confidences > conf_thr)]

if (TRUE) {
  net2_rand <- read.delim(paste0(inferelator_output, "/rand", "/network.tsv.gz"), header = TRUE, sep = "\t")
  
  net2_rand$Type <- c("Randomized")
  
  net2 <- rbind(net2, net2_rand)
}

if (TRUE) {
  net2_cv <- read.delim(paste0(inferelator_output, "/cv", "/network.tsv.gz"), header = TRUE, sep = "\t")
  
  tp <- 0
  fp <- 0
  for (i in 1:nrow(net2_cv)) {
    if (net2_cv$prior[i] == 1) {
      tp <- tp + 1
    } else {
      fp <- fp + 1
    }
    net2_cv$precision[i] <- tp/(tp + fp)
    net2_cv$recall[i] <- tp/sum(prior)
  }
  
  net2_cv$Type <- c("Holdout set")
  
  net2 <- rbind(net2, net2_cv)
}

plots <- list()

plots[[1]] <- net2 %>%
  drop_na(recall) %>%
  ggplot() +
  geom_point(aes(x = recall, y = precision, col = Type)) +
  geom_vline(xintercept=recall_thr, linetype="dashed", color = "grey", size=1.5) +
  labs(x = "Recall", y = "Precision")

plots[[2]] <- net2 %>%
  drop_na(recall) %>%
  ggplot() +
  geom_point(aes(x = combined_confidences, y = MCC, col = Type)) +
  geom_vline(xintercept=conf_thr, linetype="dashed", color = "grey", size=1.5) +
  labs(x = "Confidence score", y = "MCC")

plots[[1]]

net2 <- net2 %>%
  filter(Type == "Final network") %>%
  select(-Type)

```

All interactions in the network (i.e. predicted interactions), with comulative precision and recall. The table is sorted by confidence score so as the confidence decreases one expect precision to decrease and recall to increase. The column "prior" indicate whether the interaction was in the prior (1) or not (0).

```{r, message=FALSE, warning=FALSE}

net2 <- net2 %>%
  filter(combined_confidences >= conf_thr)

no_TFs2 <- length(unique(net2$regulator))
no_targets2 <- length(unique(net2$target))
cat("Confidence score >", format(conf_thr, digits = 3),": Network: TFs:", no_TFs2, " - Targets:", no_targets2, "\n")

net2 <- left_join(net2, gene.info, by = "regulator")
net2 <- net2 %>% rename(confidence = combined_confidences)

datatable(net2[,-c(4,8,10,11, 12)], rownames = FALSE, filter = "top",
          options = list(
            columnDefs = list(list(className = 'dt-center', targets = "_all"))
            )
          )

if (TRUE) { # Print network
  
  dap_net <- net %>%
    ungroup() %>%
    select(DAPseq.TF, Potra02.TF, Potra02.Target) %>%
    mutate(Interaction = paste(Potra02.TF, Potra02.Target, sep = "-"))
  
  inf_net <- net2 %>%
    mutate(Interaction = paste(regulator, target, sep = "-"))
  
  length(unique(c(dap_net$Interaction, inf_net$Interaction)))

  out_net <- full_join(dap_net, inf_net, by = "Interaction") %>%
    select(Interaction, confidence, gold_standard) %>%
    separate(Interaction, into = c("Regulator", "Target"), sep = "-") %>%
    mutate(gold_standard = ifelse(is.na(gold_standard), -1, gold_standard)) %>%
    mutate(confidence = ifelse(is.na(confidence), 0, confidence))
    
  write.table(out_net, quote = FALSE, row.names = FALSE, file = "dap_inferelator_network.txt")
  
  load(file = "gene.cluster.RData")
  gene.clusters <- gene.clusters %>% dplyr::rename(Gene = Genes)
  
  nodes <- rbind(data.frame(Gene = unique(out_net$Regulator), Type = c("TF")),
                 data.frame(Gene = unique(out_net$Target), Type = c("Gene"))) %>%
    filter(!(Gene %in% out_net$Regulator & Type == "Gene")) %>%
    mutate(regulator = Gene) %>%
    left_join(gene.info, by = "regulator") %>%
    select(-regulator) %>%
    left_join(gene.clusters, by = "Gene")
  
  nodes$Colors <- gsub("lightsteelblue3", "#A2B5CD", nodes$Colors)
  nodes$Colors <- gsub("mediumpurple3", "#8968CD", nodes$Colors)
  nodes$Colors <- gsub("orange", "#FFA500", nodes$Colors)
 
  write.table(nodes, quote = FALSE, row.names = FALSE, file = "dap_inferelator_nodes.txt", sep = "\t")  
}

```

TFs in network sorted by centrality:

```{r, message=FALSE, warning=FALSE}

net3 <- net2 %>%
  group_by(regulator, descr) %>%
  summarise(centrality = n(), precision = sum(prior == 1)/n(), 
            recall = sum(prior == 1)/sum(net$Potra02.TF == unique(regulator))) %>%
  arrange(desc(centrality)) %>%
  mutate(precision = format(precision, digits = 3),
         recall = format(recall, digits = 3))

datatable(net3, rownames = FALSE, filter = "top",
          options = list(
            columnDefs = list(list(className = 'dt-center', targets = "_all"))
            )
          )
```

Plots of the expession profile and activity profile of the hub TFs in the network. The heatmap shows the expression of the predicted target genes.

```{r, message=FALSE, warning=FALSE, fig.show='hide'}

topTFs <- net2 %>%
  group_by(regulator) %>%
  mutate(N = n()) %>%
  slice(1) %>%
  ungroup() %>%
  arrange(desc(N)) %>%
  pull(regulator)

tfa <- t(read.delim(paste0(inferelator_output, "/timeless/tfa.tsv"), header = TRUE, sep = "\t", row.names = 1))
tfa <- cbind(data.frame(Genes = rownames(tfa)), tfa) %>%
  gather (Samples, Activity, -1) %>%
  separate(Samples, into = c("Trees", "Samples"), sep = "\\.") %>% # - or .
  mutate_at("Samples", as.numeric)

plots <- list()
for (i in 1:10) {
  
  plots[[length(plots)+1]] <- expr %>% filter(Genes %in% topTFs[i]) %>%
    ggplot(aes(x = Samples, y = Expression, col = Genes)) +
    geom_line() +
    geom_vline(data = reprog_events, mapping = aes(xintercept = xintercept), 
               linetype="dashed", color = "gray68", size=0.5) +
    facet_grid(rows = vars(Genes), cols = vars(Trees)) +
    theme(strip.text = element_text(size = 8, color = "black"), legend.position = "none")
  
    plots[[length(plots)+1]] <- tfa %>% filter(Genes %in% topTFs[i]) %>% 
      ggplot(aes(x = Samples, y = Activity, col = Trees)) +
      geom_line() +
      geom_vline(data = reprog_events, mapping = aes(xintercept = xintercept), 
                 linetype="dashed", color = "gray68", size=0.5) +
      facet_grid(rows = vars(Genes), cols = vars(Trees)) +
      theme(strip.text = element_text(size = 8, color = "black"), legend.position = "none")
  
   targets <- net2 %>%
    filter(regulator == topTFs[i]) %>%
    pull(unique(target))
   
expr_table <- expr %>%
  filter(Genes %in% targets) %>%
  mutate_at("Samples", as.character) %>%
  rowwise() %>%
  mutate(Samples = ifelse(nchar(Samples) < 2, paste0("0",Samples), Samples)) %>%
  mutate(Samples = paste(Trees, Samples, sep = ".")) %>%
  mutate(Samples = factor(Samples, levels = samples_order)) %>%
  select(-Trees) %>%
  spread(Samples, Expression) %>%
  as.data.frame() %>%
  `rownames<-`(.[,1]) %>%
  select(-Genes)
    
    rdist <- as.dist(1-cor(t(expr_table), method="pearson"))
    rhc <- hclust(rdist,method="ward.D")
    
    plots[[length(plots)+1]] <- pheatmap(mat = as.matrix(expr_table), 
            cluster_rows = rhc,
            cluster_cols = FALSE,
            treeheight_row = 0,
            scale = "row",
            legend = FALSE,
            border_color = NA,
            color = colorRampPalette(c("dodgerblue","white","firebrick"))(10),
            fontsize = 8,
            fontsize_row = 8,
            fontsize_col = 10,
            srtCol = 45,
            show_rownames = FALSE,
            show_colnames = FALSE,
            gaps_col = c(25, 51, 79)
  )$gtable
}

```

```{r, message=FALSE, warning=FALSE, fig.height=30, fig.width=10}

cowplot::plot_grid(plotlist = plots, ncol = 3) # TFa = 3, else 2

```

#### Regulatory complexity

```{r, message=FALSE, warning=FALSE}

net2 %>%
  filter(confidence >= conf_thr) %>%
  group_by(target) %>%
  summarise(regulators = n()) %>%
  mutate_at("regulators", as.factor) %>%
  ggplot(aes(x = regulators)) +
  geom_bar() +
  labs(x = "Number of regulators", y = "Number of targets")
  
```